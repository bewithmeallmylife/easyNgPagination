/**
 * @author wenfeng.xu
 * version: 1.0.0
 * wechat:italybaby
 * 
 */
angular.module('easy.ng.pagination', []).directive('easyNgPagination', [function () {
    return {
        restrict: 'EA',
        templateUrl: 'plugin/easy-ng-pagination/tpl/pagination.html',
        replace: true,
        scope:true,
        link: function (scope, element, attrs) {
        	
           	var options=scope.easyNgPagination.options;
        	
            /**
             * 获取总页数
             */
           	scope.getTotalPage=function(totalCount,pageSize){
        		    var totalCount=(totalCount==undefined)?0:totalCount;
            	    var pageSize=(pageSize==undefined||pageSize==0)?10:pageSize;
            	    var totalPage=0;
            	    if(totalCount==0){
            		  return 0;
            	    }
            	    //如果每页大小大于总条数，那么只有一页
            		if(pageSize>=totalCount){
                		return 1;
                	}
            		//如果每页大小大于总条数，那么只有一页
                   	if(pageSize<totalCount){
                		var modPage=totalCount%pageSize;
                		var page=parseInt(totalCount/pageSize);
                		if(modPage==0){
                			return page;
                		}
                		return page+1;
                	}
            	return 0;
        	};
        	
        	//当前页码栏页数列表
        	scope.pageList=[];
        	
        	//页码栏每一页的大小
         	var pageListSize=(options.pageListSize==undefined)?10:options.pageListSize;
         	
         	
           
           	
           	/**
           	 * 更新数据索引
           	 */
        	scope.updateItemsIndex=function(){
           		scope.fromIndex=(options.currentPage-1)*options.pageSize+1;
           		var totalPage=scope.getTotalPage(options.totalCount,options.pageSize);
           		if(totalPage==options.currentPage){
           			scope.toIndex=options.totalCount;
           			return;
           		}
        		scope.toIndex=scope.fromIndex+options.pageSize-1;
        	};
        	
        	
        	
        	/**
           	 * 跳转到页面
           	 */
        	scope.goToPage=function(pageIndex){
           		options.currentPage=pageIndex;
           		scope.updateItemsIndex();
        	};
        	
        	/**
           	 * 上一页
           	 */
        	scope.prePage=function(){
        		var currentPage=options.currentPage;
        		var prePage=currentPage-1;
        		if(scope.isCurrentPageInPageList(prePage)==false){
        			if(scope.currentPageListPage>1){
        				//当前页码栏往前翻一页
            			scope.currentPageListPage=scope.currentPageListPage-1;
              			
            		}
               	}
        		if(currentPage>1){
        			options.currentPage=currentPage-1;
           		}
        		scope.updateItemsIndex();
        	};
        	
        	/**
           	 * 下一页
           	 */
            scope.nextPage=function(){
            	var totalPage=scope.getTotalPage(options.totalCount,options.pageSize);
            	var currentPage=options.currentPage;
            	var nextPage=currentPage+1;
            	//如果当前面码不在页码栏里，则把页码栏翻页
               	if(scope.isCurrentPageInPageList(nextPage)==false){
               		if(scope.currentPageListPage<scope.totalPageListPage){
               		    //当前页码栏往后翻一页
            			scope.currentPageListPage=scope.currentPageListPage+1;
               		}
               	}
               	if(currentPage<totalPage){
        		    options.currentPage=currentPage+1;
           		}
               	scope.updateItemsIndex();
        	};
        	
        	/**
        	 * 
        	 * 更新页码栏（对页码再次进行分页）
        	 * 比如一共有100页，但当前只显示最多10页的分页数，而不是把100页的页数都显示在页码栏
        	 * 
        	 */
        	scope.updatePageList=function(){
        		
        		//数据总页数
        		var totalPage=scope.getTotalPage(options.totalCount,options.pageSize);
        		
        		//页码栏的总页数
        		var totalPageListPage=scope.getTotalPage(totalPage,pageListSize);
        		
        		scope.totalPageListPage=totalPageListPage;
        		
        		//初始化页码栏的当前页数为1
        		if(scope.currentPageListPage==undefined){
     	    		scope.currentPageListPage=1;
         		}
        		
				//清空当前页码列表
           		scope.pageList=[];
				   
         	    //当页码栏每页大小大于等于数据分页总页数
         		if(pageListSize>=totalPage){
         			for(var i=0;i<totalPage;i++){
             			scope.pageList.push(i+1);
             		}
         			return ;
         		}
        		
         		//页码栏页码起始页数
        	    var fromPageListPage=(scope.currentPageListPage-1)*pageListSize+1;
        		
        	    //页码栏页码结束页数
        		var toPageListPage=0;
        		
        		//页码栏页数求余数
        		var totalPageListPageRemainder=totalPage%pageListSize;
        		
        	    //如果当前页码栏页数=页码栏总页数，并且页码栏页数求余数不为0
           		if(scope.currentPageListPage==totalPageListPage&&totalPageListPageRemainder!=0){
         			
        			toPageListPage=fromPageListPage+totalPageListPageRemainder;
        			
        		}else{
        			
        			toPageListPage=fromPageListPage+pageListSize;
        			
        		}
            	
           		
           		//刷新页码栏页码
    			for(var i=fromPageListPage;i<toPageListPage;i++){
       				scope.pageList.push(i);
       			}
    			
        	};
        	
        	/**
        	 * 页码栏向上翻一页
        	 */
        	scope.prePageListPage=function(){
        		if(scope.currentPageListPage>1){
        			scope.currentPageListPage=scope.currentPageListPage-1;
          			scope.updatePageList();
        		}
           	}
        	/**
        	 * 页码栏向下翻一页
        	 */
            scope.nextPageListPage=function(){
               	if(scope.currentPageListPage<scope.totalPageListPage){
        			scope.currentPageListPage=scope.currentPageListPage+1;
           			scope.updatePageList();
        		}
           	}
            /**
        	 * 当前页是否在页码栏中
        	 */
            scope.isCurrentPageInPageList=function(currentPage){
               	for(i=0;i<scope.pageList.length;i++){
            		if(scope.pageList[i]==currentPage){
            			return true;
            		}
               	}
            	return false;
            }
            
             /**
             * 当页码变化时执行的操作
             */
            scope.$watch('easyNgPagination.options.currentPage',function(){
                options.onPageChange();
            });

            /**
            * 查询返回结果时执行的操作
            */
            scope.$watch('items',function(){
                scope.updateItemsIndex();
                scope.updatePageList();
            });
        	
        	
        }
    };
}]);